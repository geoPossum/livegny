#include <stdio.h>
#include <ao/ao.h>
#include <sndfile.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char argv[]){

		
	//libsnd open the file
	char path[] = "ringfinger.wav";
	int mode = SFM_READ;
	SF_INFO sfinfo;
	memset(&sfinfo, 0, sizeof(sfinfo));
	SNDFILE* sndfile = sf_open(path, mode, &sfinfo);

	int bpm, bar, repeat=1, bits=16, end_loop=3, start_loop=2;
	float bps;
	bpm = 78;
	bps = (float)bpm/60;
	bar = 8;
	float frames_per_bar = bar*2*44100/bps;
	printf("bpm:\t%d\nbps:\t%f\nframes_per_bar:\t%f\n",bpm,bps,frames_per_bar);
	
	if (sndfile == NULL){
		printf("Error Opening Filei\n");
		goto end;
	}
	printf("\nOpened File:\t%s\n\t\t%s:\t\t%d, \n\t\t%s:\t%d, \n\t\t%s:\t%d, \n\t\t%s:\t%d, \n\t\t%s:\t%s \n",path,\
			"Frames",(int)sfinfo.frames,"SampleRate",sfinfo.samplerate,\
			"Channels", sfinfo.channels, "Sections", sfinfo.sections,  "Seekable",sfinfo.seekable?"True":"False");
	{
		int format = sfinfo.format & 7;	
		char *format_string = malloc(39);
		strcpy(format_string,"8-Bit");
		switch (format){
			case 1: 
				strcpy(format_string,"8-Bit");
				bits = 8;
				break;
			case 2:
				strcpy(format_string, "16-Bit");
				bits = 16;
				break;
			case 3:
				strcpy(format_string, "24-Bit");
				bits = 24;
				break;
			case 4:
				strcpy(format_string,"32-Bit");
				bits = 32;
				break;
			default:
				break;
		}
		printf("\t\t%s:\t\t%s\n","Format", format_string);
	}
	//libao initialise device
	ao_initialize();

	//libao default driver
	int driver_id = ao_default_driver_id();
	if (-1 == driver_id){
		perror("Unable to get a driver");
		exit(-1);
	}
	ao_sample_format format;
	format.channels = sfinfo.channels;
	format.rate = sfinfo.samplerate;
	format.bits = bits;
	ao_device *device = ao_open_live(driver_id, &format, NULL);
	if (device == NULL){
		perror("Error with device opening");
		goto end;
	}

	//lism read one second
	/*sf_count_t frames_to_read = 10 * sfinfo.samplerate * sfinfo.channels * 2;*/
	sf_count_t frames_to_read = (int)frames_per_bar; 
	void *my_frames = calloc( frames_to_read, sizeof(char));
	/*printf("Allocated %d at %p\n",(int)frames_to_read, my_frames);*/
	if (NULL  == my_frames){
		printf("Unable to allocate memory for reading\n");
		free(my_frames);
		goto end;
	}
	/*Frames to read must always be an integer multiple of no of channels x bitspersample*/
	frames_to_read = sfinfo.channels*bits*(frames_to_read/(sfinfo.channels*bits));
	int count=1;

	while(1){
		sf_seek(sndfile, (count++-1)*frames_to_read/4, SEEK_SET);
		printf("%c",'-');
		fflush(stdout);
		int read_frames =sf_read_raw(sndfile, my_frames, frames_to_read)  ;
		/*printf("Read %d out of %d frames.\n",read_frames, (int)frames_to_read);*/
		if ( read_frames!= frames_to_read ){
			printf("Did not read all %d frames. Read %d frames.\n",(int)frames_to_read, read_frames);
			if (0 == read_frames){
				break;
			}
		}	
	
		//libao play
		if ( 0 == ao_play(device, (char *)my_frames, read_frames)){
			perror("Unable to Play");
		free(my_frames);
		goto end;
		}

		//repeat between bars 2 and 5
		if ((repeat) && (count>end_loop))count=start_loop;
	}
	free(my_frames);
	
	end:
	ao_shutdown();
	printf("Ending\n");
	return 0;
}
