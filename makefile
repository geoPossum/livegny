CC = gcc
CFLAGS = `pkg-config --cflags --libs ao`  -lsndfile 

myaudio: myaudio.c  
	$(CC) myaudio.c -o myaudio $(CFLAGS)

mysnd: mysnd.c
	$(CC) mysnd.c -L/usr/lib/x86_64-linux-gnu/ -lsndfile -o mysnd 

all: mysnd myaudio
